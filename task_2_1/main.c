/*
 * main.c
 *
 *  Created on: 19.04.2016
 *      Author: Grishma
 */

#include<ses_led.h>
#include<util/delay.h>
#include<ses_lcd.h>
#include<ses_uart.h>

int main(void) {
	led_greenInit();
	led_redInit();
	led_yellowInit();
	lcd_init();
	uart_init(57600);
	while (1) {

		lcd_setCursor(1, 1);
		fprintf(lcdout, "START\n");
		led_greenOn();
		lcd_setCursor(1, 1);
		fprintf(lcdout, "GREEN\n");
		_delay_ms(5000);
		led_greenOff();
		lcd_clear();
		led_yellowOn();
		lcd_setCursor(1, 1);
		fprintf(lcdout, "YELLOW\n");
		_delay_ms(500);
		led_yellowOff();
		lcd_clear();
		led_redOn();
		lcd_setCursor(1, 1);
		fprintf(lcdout, "RED\n");
		_delay_ms(2000);
		led_redOff();
		lcd_clear();
		led_redOn();
		led_yellowOn();
		lcd_setCursor(1, 1);
		fprintf(lcdout, "YELLOW\n");
		_delay_ms(300);
		led_redOff();
		led_yellowOff();
		lcd_clear();

		led_greenOn();
		_delay_ms(5000);
		lcd_setCursor(1, 1);
		fprintf(lcdout, "GREEN\n");
		led_greenOff();
		lcd_clear();

	}
	return 0;
}
