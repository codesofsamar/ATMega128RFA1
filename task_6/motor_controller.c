#include<ses_button.h>
#include<ses_led.h>
#include<ses_lcd.h>
#include<ses_uart.h>
#include<ses_adc.h>
#include<stdlib.h>
#include<ses_timer.h>
#include "ses_scheduler.h"
#include <util/atomic.h>
#include "ses_pwm.h"
#include "ses_common.h"
#include "ses_motorFrequency.h"

#define Period_task_debounce 5
bool motor_state = false;

/* toggle the state of the motor using a flag*/
void start_motor() {
	if (!motor_state) {
		pwm_setDutyCycle(170);
		motor_state = true;
	} else {
		pwm_setDutyCycle(0);
		motor_state = false;
	}

}

/**
 * to avoid debouncing of buttons we check the button states
 */
void task_button_debouncing(void *a) {
	button_checkState();
}

/**
 * prints the instantaneous RPM
 * and median RPM to the screen
 */
void print_revolutions(void *a) {

	lcd_clear();

	uint16_t myRecent = motorFrequency_getRecent();
	uint16_t myMedian = motorFrequency_getMedian();

	/* we multiply the values with 60 to get the RPM*/
	lcd_setCursor(0, 0);
	fprintf(lcdout, "recent2/min: %u\n", myRecent * 60);

	lcd_setCursor(0, 2);
	fprintf(lcdout, "Median/min : %u\n", myMedian * 60);

	lcd_setCursor(0, 1);
	fprintf(lcdout, "myBool %d", returnMybool());

}

int main(void) {

	pwm_init();
	led_redInit();
	led_greenInit();
	led_yellowInit();

	led_greenOff();
	led_yellowOff();
	led_redOff();

	pwm_setDutyCycle(0);
	button_init(true);
	scheduler_init();
	motorFrequency_init();
	lcd_init();
	timer5_init();

	/* task that checks the button state every 5ms*/
	struct taskDescriptor_s task_debounce;
	task_debounce.execute = 0;
	task_debounce.period = Period_task_debounce;
	task_debounce.task = task_button_debouncing;
	task_debounce.expire = Period_task_debounce;
	scheduler_add(&task_debounce);

	/* task to show the no. of rotations per minute */
	struct taskDescriptor_s task_print_revolutions;
	task_print_revolutions.execute = 0;
	task_print_revolutions.period = 1000;
	task_print_revolutions.expire = 1000; //every 1 second
	task_print_revolutions.task = print_revolutions;
	scheduler_add(&task_print_revolutions);

	/* call back to set duty cycle and toggle the state of motor*/

	button_setRotaryButtonCallback(start_motor);

	/* enable the global interrupts */
	sei();

	for (;;) {
		scheduler_run();
	}

	return 0;
}

