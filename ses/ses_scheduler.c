/*INCLUDES ************************************************************/
#include "ses_timer.h"
#include "ses_scheduler.h"
#include "ses_led.h"
#include "util/atomic.h"

/* PRIVATE VARIABLES **************************************************/
/** list of scheduled tasks */
static taskDescriptor* taskList = NULL;

volatile static systemTime_t systemTime = 0;

//struct time_t userTime = {.hour = 0, . minute = 0, .milli = 0};

void scheduler_setTime(systemTime_t time) {
	systemTime = time;
}

systemTime_t scheduler_getTime() {
	return systemTime;
}

/*FUNCTION DEFINITION *************************************************/
static void scheduler_update(void) {

	/*
	 * since this block is updated every 1 ms
	 * the systemTime will have a precision of 1ms
	 */

		systemTime++;

	taskDescriptor *cursor;
	cursor = taskList;
	while (cursor != NULL) {
		//i removed the atomic block from here
		if (cursor->execute != 1) {
			cursor->expire--;

			/* resetting the period of the periodic task */
			if (cursor->expire == 0) {
				cursor->execute = 1;
				cursor->expire = cursor->period;
			}
		}
		cursor = cursor->next;

	}
}

void scheduler_init() {
	timer2_start();
	timer2_setCallback(scheduler_update);
}

void scheduler_run() {

	task_t taskRunner = NULL;
	taskDescriptor* cursor = NULL;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		cursor = taskList;
		while (cursor != NULL) {
			taskRunner = cursor->task;

			if (cursor->execute && taskRunner != NULL) {
				taskRunner(cursor->param);
				cursor->execute = 0;
			}
			/*
			 * delete non periodic tasks after the execution
			 */
			if (cursor->period == 0) {
				scheduler_remove(cursor);
			}
			cursor = cursor->next;
		}
	}
}

bool scheduler_add(taskDescriptor * toAdd) {

	if (toAdd == NULL) {
		return false;
	}

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		taskDescriptor *cursor = NULL;
		cursor = taskList;
		/*
		 * if the list is empty
		 * it becomes the head insertion
		 */
		if (cursor == NULL) {
			toAdd->next = NULL;
			taskList = toAdd;
			cursor->execute = 0; //changes from the feedback
			return true;
		} else {
			/*
			 * go to the end and add it there
			 */
			while (cursor->next != NULL) {
				if (cursor == toAdd) {
					return false;
				}
				cursor = cursor->next;
			}
			cursor->next = toAdd;
			toAdd->next = NULL;
			cursor->execute = 0;
//			return true;

		}
	}
	return true;
}

void scheduler_remove(taskDescriptor * toRemove) {
	taskDescriptor *cursor;
	taskDescriptor *temp;
	cursor = taskList;

	/*
	 * check if it is the only task in the schedule
	 */
	if (taskList == toRemove) {

		taskList = taskList->next;
		return;
	}

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		while (cursor->next != NULL) {
			if (cursor->next == toRemove) {
				temp = cursor->next;
				cursor->next = temp->next;
				return;
			}
			cursor = cursor->next;
		}
	}
}
