#ifndef SES_MOTORFREQUENCY_H_
#define SES_MOTORFREQUENCY_H_

/**
 * Enables interrupt on a rising edge
 * external pin interrupt is activated
 */
void motorFrequency_init();

/**
 * Returns the most recent Frequency of the motor
 * @return uint16_t
 */
uint16_t motorFrequency_getRecent();

/**
 * Returns the median of the array that stores recent frequnecies
 * @return uint16_t
 */
uint16_t motorFrequency_getMedian();

/**
 * initalizes the timer 5 which we use
 * for frequency counting
 */
void timer5_init();

/**
 * this function adds the recent frequency of the motor
 * to the array (in the begining). the array is used for
 * median calculation
 * @param uint16_t element to add to the array
 */

void update_array(uint16_t a);

/**
 * this function returns a variable that i used
 * for debugging
 * @return bool
 */
bool returnMybool();

#endif /* SES_MOTORFREQUENCY_H_ */
