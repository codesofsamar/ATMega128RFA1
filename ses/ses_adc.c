/*
 * ses_adc.c
 *
 *  Created on: 26.04.2016
 *      Author: Grishma
 */

#include "ses_button.h"
#include "ses_led.h"
#include "ses_common.h"
#include "ses_lcd.h"
#include "ses_button.h"
#include "ses_adc.h"
#include<avr/io.h>
#include<avr/sleep.h>
#include<util/atomic.h>

// sensor ports and pins

#define ADC_LIGHT_PORT PORTF
#define ADC_LIGHT_PIN 2

#define ADC_TEMP_PORT PORTF
#define ADC_TEMP_PIN 4

#define ADC_JOYSTICK_PORT PORTF
#define ADC_JOYSTICK_PIN  5

#define ADC_MIC_NEG_PORT PORTF
#define ADC_MIC_NEG_PIN  0

#define ADC_MIC_POS_PORT PORTF
#define ADC_MIC_POS_PIN 1

#define VALUES 20
const uint8_t SCALE_FACTOR = 100;

#define ADC_TEMP_FACTOR 10
#define ADC_TEMP_MAX 40*ADC_TEMP_FACTOR
#define ADC_TEMP_MIN 20*ADC_TEMP_FACTOR

#define ADC_TEMP_RAW_MAX 481
#define ADC_TEMP_RAW_MIN 256

const uint8_t THRESHOLD = 50;

void adc_init(void) {

	/*	Configuration of DDR registers */
	/*	deactivation of pull-up registers */
	DDR_REGISTER(ADC_LIGHT_PORT) &= ~(1 << ADC_LIGHT_PIN);
	DDR_REGISTER(ADC_TEMP_PORT) &= ~(1 << ADC_TEMP_PIN);
	DDR_REGISTER(ADC_JOYSTICK_PORT) &= ~(1 << ADC_JOYSTICK_PIN);
	DDR_REGISTER(ADC_MIC_POS_PORT) &= ~(1 << ADC_MIC_POS_PIN);
	DDR_REGISTER(ADC_MIC_NEG_PORT) &= ~(1 << ADC_MIC_NEG_PIN);

	ADC_LIGHT_PORT &= ~(1 << ADC_TEMP_PIN);
	ADC_TEMP_PORT &= ~(1 << ADC_LIGHT_PIN);
	ADC_MIC_NEG_PORT &= ~(1 << ADC_MIC_NEG_PIN);
	ADC_MIC_POS_PORT &= ~(1 << ADC_MIC_POS_PIN);
	ADC_JOYSTICK_PORT &= ~(1 << ADC_JOYSTICK_PIN);

	/*disable power reduction mode in ADC */
	PRR0 &= ~(1 << PRADC);

	/* reference voltage set */
	ADMUX |= (ADC_VREF_SRC << REFS0);

	/* configuring the ADLAR bit */
	ADMUX &= ~(1 << ADLAR);

	ADCSRA = ((1 << ADEN) | (0 << ADSC) | (0 << ADATE) | (1 << ADIF)
			| (1 << ADIE) | (ADC_PRESCALE << ADPS0));

	/* The minimum required ADC start-up time is 20 micro seconds */
	_delay_us(20);

}

uint16_t adc_read(uint8_t adc_channel) {

	ADCSRB &= ~(1 << MUX5);
	ADMUX &= ~((1 << MUX4) | (1 << MUX3) | (1 << MUX2) | (1 << MUX1)
			| (1 << MUX0));

	if (adc_channel < ADC_NUM) {
		ADMUX |= adc_channel;
	} else
		return ADC_INVALID_CHANNEL;

	/* start conversion */
	ADCSRA |= 1 << ADSC;

	do {
	} while ((ADCSRA & (1 << ADSC))); // != 0);

	return ADC;
}

void adc_getJoystickDirection() {

	uint16_t rawData = adc_read(ADC_JOYSTICK_CH);
	if (abs(rawData - 200) < THRESHOLD) {
		lcd_clear();
		fprintf(lcdout, "Right");
	} else if (abs(rawData - 400) < THRESHOLD) {
		lcd_clear();
		fprintf(lcdout, "UP");
	} else if (abs(rawData - 600) < THRESHOLD) {
		lcd_clear();
		fprintf(lcdout, "Left");
	} else if (abs(rawData - 800) < THRESHOLD) {
		lcd_clear();
		fprintf(lcdout, "DOWN");
	} else if (abs(rawData - 1000) < THRESHOLD) {
		lcd_clear();
		fprintf(lcdout, "No direction");
	}

}

int16_t adc_getTemperature() {

	int16_t adc = adc_read(ADC_TEMP_CH);
	int16_t slope = (ADC_TEMP_MAX - ADC_TEMP_MIN)
			/ (ADC_TEMP_RAW_MAX - ADC_TEMP_RAW_MIN);
	int16_t offset = ADC_TEMP_MAX - (ADC_TEMP_RAW_MAX * slope);
	return (adc * slope + offset) / ADC_TEMP_FACTOR;

}

