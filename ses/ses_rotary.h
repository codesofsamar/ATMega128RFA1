

#ifndef SES_ROTARY_H_
#define SES_ROTARY_H_

typedef void (*pTypeRotaryCallback)();

/*
 * Initializes rotary encoder
 */
void rotary_init();

/*
 * Assigns corresponding function on clockwise rotation of rotary encoder
 */
void rotary_setClockwiseCallback(pTypeRotaryCallback);

/*
 * Assigns corresponding function on anti clockwise rotation of rotary encoder
 */
void rotary_setCounterClockwiseCallback(pTypeRotaryCallback);

void check_rotary();

void Rotary_Debounce();


#endif /* SES_ROTARY_H_ */
