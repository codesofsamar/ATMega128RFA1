/*
 * ses_button.c
 *
 *  Created on: 26.04.2016
 *      Author: Grishma
 */
#include "ses_common.h"
#include "ses_button.h"
#include "ses_timer.h"

pButtonCallback joystickCallback = NULL;
pButtonCallback rotaryCallback = NULL;

#define BUTTON_NUM_DEBOUNCE_CHECKS	5

#define BUTTON_JOYSTICK_PORT PORTB
#define BUTTON_JOYSTICK_PIN  7

#define BUTTON_ROTARY_PORT PORTB
#define BUTTON_ROTARY_PIN  6

void button_init(bool debouncing) {

	DDR_REGISTER(BUTTON_ROTARY_PORT) &= ~(1 << BUTTON_ROTARY_PIN);

	DDR_REGISTER(BUTTON_JOYSTICK_PORT) &= ~(1 << BUTTON_JOYSTICK_PIN);

	BUTTON_JOYSTICK_PORT |= (1 << BUTTON_JOYSTICK_PIN);

	BUTTON_ROTARY_PORT |= (1 << BUTTON_ROTARY_PIN);

	if (debouncing) {

		/* this was for the previous tasks */
//		timer1_start();
//		timer1_setCallback(button_checkState);
	} else {

		PCMSK0 |= (1 << BUTTON_ROTARY_PIN) | (1 << BUTTON_JOYSTICK_PIN);
		/* enable the pin change interrupt */
		PCICR = 1 << PCIE0;
	}

}

bool button_isJoystickPressed(void) {
	if (PIN_REGISTER(BUTTON_JOYSTICK_PORT) & ((1 << BUTTON_JOYSTICK_PIN))) {
		return false;
	} else {
		return true;
	}
}

bool button_isRotaryPressed(void) {
	if (PIN_REGISTER(BUTTON_ROTARY_PORT) & ((1 << BUTTON_ROTARY_PIN))) {
		return false;
	} else {
		return true;
	}
}

/* functions for task_4 */
void button_setRotaryButtonCallback(pButtonCallback callback) {
	if (callback != NULL) {
		rotaryCallback = callback;
	}
}

void button_setJoystickButtonCallback(pButtonCallback callback) {
	if (callback != NULL) {
		joystickCallback = callback;
	}
}

void button_checkState() {
	static uint8_t state[BUTTON_NUM_DEBOUNCE_CHECKS] = { };
	static uint8_t index = 0;
	static uint8_t debouncedState = 0;
	uint8_t lastDebouncedStates = debouncedState;

	/* each bit in every state byte represents one button */
	state[index] = 0;
	if (button_isJoystickPressed()) {
		state[index] |= 1;
	}
	if (button_isRotaryPressed()) {
		state[index] |= 2;
	}

	index++;
	/* check to make sure that the index hasn't gone over the limit */
	if (index == BUTTON_NUM_DEBOUNCE_CHECKS) {
		index = 0;
	}

	// we read BUTTON_NUM_DEBOUNCE_CHECKS consistent "0" in the state
	// array, the button at this position is considered pressed
	uint8_t j = 0xFF;
	uint8_t changed = 0x00;
	for (uint8_t i = 0; i < BUTTON_NUM_DEBOUNCE_CHECKS; i++) {
		j = j & state[i];
	}
	debouncedState = j;
	/**
	 * calculate what changed
	 * If the switch was high and is now low, 1 and 0 xORed with
	 * it is 0
	 */

	changed = debouncedState ^ lastDebouncedStates;

	if ((changed & 1) && button_isJoystickPressed()) {
		/* joystick callback */
		if (joystickCallback != NULL) {
			joystickCallback();
		}
	}
	if ((changed & 2) && button_isRotaryPressed()) {
		/* rotary callback */
		if (rotaryCallback != NULL) {
			rotaryCallback();
		}
	}
}

/* interrupt service routine */
ISR(PCINT0_vect) {
	/* check whether one of the button values changes */
	if ((PIN_REGISTER(BUTTON_ROTARY_PORT) & (1 << BUTTON_ROTARY_PIN)) == 0) {
		if (rotaryCallback != NULL) {
			rotaryCallback();
		}
	}
	if ((PIN_REGISTER(BUTTON_JOYSTICK_PORT) & (1 << BUTTON_JOYSTICK_PIN))
			== 0) {
		if (joystickCallback != NULL) {
			joystickCallback();
		}
	}
}
