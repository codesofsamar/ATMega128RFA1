#ifndef SES_PWM_H_
#define SES_PWM_H_
#include "ses_common.h"
#include "ses_button.h"

/* FUNCTION PROTOTYPES *******************************************************/
//typedef void (*pwmButtonCallback)();
//typedef void (*pButtonCallback)();

/**
 * Initializes the timer 0
 * and sets the PORTB pin 6 as output
 */
void pwm_init(void);

/**
 *	Provides the duty cycle to the motor
 *	@param uint8_t the dutycycle value that you want to set
 */
void pwm_setDutyCycle(uint8_t dutyCycle);


#endif /* SES_PWM_H_ */
