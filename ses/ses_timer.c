/* INCLUDES ******************************************************************/
#include "ses_timer.h"

/* DEFINES & MACROS **********************************************************/
#define TIMER1_CYC_FOR_5MILLISEC	1249
#define TIMER2_CYC_FOR_1MILLISEC	249

pTimerCallback timer2_callback = NULL;
pTimerCallback timer1_callback = NULL;

/*FUNCTION DEFINITION ********************************************************/
void timer2_setCallback(pTimerCallback cb) {
	timer2_callback = cb;
}

void timer2_start() {
	/* use clear timer on Compare Match */
	TCCR2A |= (1 << WGM21);

	/* select a prescaler of 64 */
	TCCR2B |= (1 << CS22);

	/* set interrupt mask register for compare A */
	TIMSK2 |= (1 << OCIE2A);

	/* clear the interrupt flag by setting an 1 in register for Compare A */
	TIFR2 |= (1 << OCF2A);
	/* set a value in register OCR2A in order to generate an interrupt every 1 ms */
	OCR2A = TIMER2_CYC_FOR_1MILLISEC;

}

void timer2_stop() {
	/* clearing the bits that we set in the init*/
	TCCR2A &= ~(1 << WGM21);
	TCCR2B &= ~(1 << CS22);
}

void timer1_setCallback(pTimerCallback cb) {
	timer1_callback = cb;
}

void timer1_start() {

	/* set a prescaler first prescale of 64
	 * use clear timer on Compare Match
	 */
	TCCR1B |= (1 << CS11) | (1 << WGM12) | (1 << CS10);

	/*
	 *  clear the interrupt flag by setting an 1 in register for Compare A
	 *  TIFR is common register for all the registers
	 */
	TIFR1 |= (1 << OCF1A);

	/*set interrupt mask register for compare A*/
	TIMSK1 |= (1 << OCIE1A);

	/* set a value in register OCR1A in order to generate an interrupt every 5 ms */
	OCR1A = TIMER1_CYC_FOR_5MILLISEC;
}

void timer1_stop() {
	TCCR1B &= ~(1 << CS10);
	TCCR1B &= ~(1 << WGM12);
	TCCR1B &= ~(1 << CS11);
}

/*Interrupt Service routine for timer 1*/
ISR(TIMER1_COMPA_vect) {
	if (timer1_callback != NULL) {
		timer1_callback();
	}
}

/*Interrupt Service routine for timer 2*/
ISR(TIMER2_COMPA_vect) {
	if (timer2_callback != NULL) {
		timer2_callback();
	}
}
