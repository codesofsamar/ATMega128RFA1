#include "ses_common.h"
#include "ses_led.h"
#include "ses_uart.h"
#include "ses_lcd.h"
#include "ses_motorFrequency.h"
#include "util/atomic.h"
#include "string.h"

#define mySize 24
int emf_counter = 0;
uint16_t temp = 0;
uint16_t myArray[mySize + 1] = { [0 ... mySize] = 0 };
uint16_t recent_ = 0;
uint16_t localArray[mySize + 1];
bool myBool = true;

void motorFrequency_init() {

	/* enable interrupt as a rising edge*/
	EICRA |= (1 << ISC01) | (1 << ISC00);

	/* external interrupt pin is activated */
	EIMSK |= (1 << INT0);

}

void timer5_init() {
	//enabling timer5
	PRR1 &= ~(1 << PRTIM5);

	/**
	 * OCIE5A is for output compare A match interrupt Enable
	 */
	TIMSK5 |= (1 << TOIE5) | (1 << OCIE5A);

	/*output compare A match flag*/
	TIFR5 |= (1 << OCF5A);

	/*
	 * initialize the counter value with 0
	 * not necessary though
	 */
	TCNT5 = 0;

	/* setting timer 5*/
	/*ctc top, OCRnA*/
	TCCR5B |= (1 << WGM52);

	/*for 64 prescaler*/
	TCCR5B |= (1 << CS50) | (1 << CS51);
	OCR5A = 24999;
}

uint16_t motorFrequency_getRecent() {

	/*
	 * when the counter overflows before
	 * the rising edge interrupt is enabled this means
	 * the motor has been stopped
	 */
	if (myBool) {
		return 0;
	} else
		return (1 / (recent_ * 4 * pow(10, -6)));
}

uint16_t motorFrequency_getMedian() {
	/*sort the values in ascending order */

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		memcpy(localArray, myArray, sizeof(myArray));
	}
	uint16_t temp = 0;
	for (int i = 0; i < mySize + 1; i++) {
		for (int j = i + 1; j < mySize + 1; j++) {
			if (localArray[i] > localArray[j]) {
				temp = localArray[i];
				localArray[i] = localArray[j];
				localArray[j] = temp;
			}
		}
	}

	/* returns the median value */
	return 1 / (localArray[mySize / 2] * 4 * pow(10, -6));
}

void updateArray(uint16_t a) {

	for (int i = 1; i <= mySize; i++) {
		myArray[i] = myArray[i - 1];
	}
	myArray[0] = a;
}

bool returnMybool() {
	return myBool;
}

/*
 * Interrupt service routine for INT0
 */
ISR(INT0_vect) {
	led_yellowToggle();
	led_greenOff();
	emf_counter++;
	/* sum the six emf interrupts */
	temp += TCNT5;
	if (emf_counter == 6) {
		recent_ = temp;
		temp = 0;
		emf_counter = 0;

		/* and add it to the array list*/
		updateArray(recent_);
	}

	TCNT5 = 0;

	myBool = false;
}

/**
 * This interrupt routine is called when the motor is rotating slower than 10Hz
 * i.e the timer5 overflows faster than the rising edge interrupt
 */
ISR(TIMER5_COMPA_vect) {
	recent_ = 0;
	myBool = true;
	led_greenOn();
	updateArray(recent_);
}

