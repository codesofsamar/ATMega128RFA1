/* INCLUDES ******************************************************************/

#include "ses_common.h"
#include "ses_led.h"
#include "ses_uart.h"
#include "ses_lcd.h"
#include "ses_pwm.h"
#include "ses_button.h"

#define PWM_PORT       	PORTG
#define PWM_PIN         	5

#define BUTTON_ROTARY_PORT PORTB
#define BUTTON_ROTARY_PIN  6


void pwm_init(void) {

	/* enable timer0*/
	PRR0 &= ~(1 << PRTIM0);

	/* fast PWM mode*/
	TCCR0A |= (1 << WGM01) | (1 << WGM00);

	/* disable the pre-scaler */
	TCCR0B |= (1 << CS00);

	/* for the compare */
	TCCR0A |= (1 << COM0B1);

	DDR_REGISTER(PWM_PORT) |= (1 << PWM_PIN);

}


void pwm_setDutyCycle(uint8_t dutyCycle) {

	OCR0B = dutyCycle;
}

