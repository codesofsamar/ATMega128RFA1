#include "ses_common.h"
#include "ses_lcd.h"
#include "ses_rotary.h"

#define A_ROTARY_PORT    PORTB
#define B_ROTARY_PORT    PORTG
#define A_ROTARY_PIN     5
#define B_ROTARY_PIN     2
#define A_CHANNEL        (PINB &(1<<A_ROTARY_PIN))
#define B_CHANNEL        (PING &(1<<B_ROTARY_PIN))

static pTypeRotaryCallback Call_Clockwise = NULL;
static pTypeRotaryCallback Call_Anticlockwise = NULL;

uint8_t position = 0;
uint8_t lastState = 0xFF;

void rotary_init() {

	DDR_REGISTER(A_ROTARY_PORT) &= ~(1 << A_ROTARY_PIN);
	DDR_REGISTER(B_ROTARY_PORT) &= ~(1 << B_ROTARY_PIN);

	A_ROTARY_PORT |= (1 << A_ROTARY_PIN);
	B_ROTARY_PORT |= (1 << B_ROTARY_PIN);
}

void rotary_setClockwiseCallback(pTypeRotaryCallback cb) {
	Call_Clockwise = cb;
}

void rotary_setCounterClockwiseCallback(pTypeRotaryCallback cb) {
	Call_Anticlockwise = cb;
}
/*
 * Callback-procedure to plot the samples of the rotary pins on the LCD after first change
 */
void check_rotary() {
	static uint8_t p = 0;
	static bool sampling = false;
	bool a = PIN_REGISTER(A_ROTARY_PORT) & (1 << A_ROTARY_PIN);
	bool b = PIN_REGISTER(B_ROTARY_PORT) & (1 << B_ROTARY_PIN);
	if (a != b)
		sampling = true;
	if (sampling && p < 122) {
		lcd_setPixel((a) ? 0 : 1, p, true);
		lcd_setPixel((b) ? 4 : 5, p, true);
		p++;
	}
}

uint8_t returnPosition() {
	return position;
}

void Rotary_Debounce() {

	if (!A_CHANNEL && lastState) {

		if (!A_CHANNEL && B_CHANNEL) {
			position++;
			if (Call_Clockwise != NULL)
				Call_Clockwise();
		} else {
			position--;
			if (Call_Anticlockwise != NULL)
				Call_Anticlockwise();

		}
	}
	lastState = A_CHANNEL;
}
