#ifndef SES_ALARM_H_
#define SES_ALARM_H_

#include "ses_common.h"
#include <stdbool.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "ses_button.h"
#include "ses_lcd.h"
#include "ses_led.h"
#include "ses_timer.h"
#include "ses_scheduler.h"

typedef struct fsm_s Fsm; //< typedef for alarm clock state machine
typedef struct event_s Event; //< event type for alarm clock fsm

/** return values */
enum {
	RET_HANDLED, //< event was handled
	RET_IGNORED, //< event was ignored; not used in this implementation
	RET_TRANSITION //< event was handled and a state transition occurred
};

/** typedef for state event handler functions */
typedef uint8_t fsmReturnStatus; //< typedef to be used with above enum

typedef fsmReturnStatus (*State)(Fsm *, const Event*);

enum {
	ENTRY, EXIT, JOYSTICK_PRESSED, ROTARY_PRESSED, ROTARY_CLOCKWISE, ROTARY_ANTICLOCKWISE, ALARM_FIRED
};

typedef struct event_s {
	uint8_t signal; //< identifies the type of event
} Event;

struct fsm_s {
	State state; //< current state, pointer to event handler
	bool isAlarmEnabled; //< flag for the alarm status
	struct time_t timeSet; //< multi-purpose var for system time and alarm time
};

/**
 * Initial state of the alarm clock
 */

void clock_init();


inline static void fsm_init(Fsm* fsm, State init);

inline static void fsm_dispatch(Fsm* fsm, const Event* event);
/* Define States */
fsmReturnStatus clockRun(Fsm *fsm, const Event *event);

fsmReturnStatus setHourClock(Fsm *fsm, const Event *event);
fsmReturnStatus setMinuteClock(Fsm *fsm, const Event *event);

fsmReturnStatus setHourAlarm(Fsm *fsm, const Event *event);
fsmReturnStatus setMinuteAlarm(Fsm *fsm, const Event *event);
fsmReturnStatus alarmFired(Fsm *fsm, const Event *event);
/**
 * gives the time to the scheduler
 */
fsmReturnStatus setTimeClock(Fsm *fsm, const Event *event);

void displayClock(struct time_t myTime, bool showSeconds);

#endif /* SES_ALARM_H_ */

