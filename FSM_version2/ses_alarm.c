/* INCLUDES ******************************************************************/

#include "ses_alarm.h"

#include <stdint.h>
#include <stdio.h>

#include "ses_rotary.h"
#include "ses_uart.h"

#define HOUR_IN_MS                     3600000
#define MIN_IN_MS                        60000
#define SEC_IN_MS                         1000
#define EXPIRY_TIME                       5000
#define BAUDRATE                         57600

#define Period_task_debounce                5
#define Period_1000ms                    1000

#define TRANSITION(newState) (fsm->state = newState, RET_TRANSITION)

Fsm *theFsm;

struct time_t alarm_time = { .hour = 0, .minute = 0, .second = 0, .milli = 0 };
struct time_t clock_time = { .hour = 0, .minute = 0, .second = 0, .milli = 0 };

struct taskDescriptor_s task_debouncing;

struct taskDescriptor_s task_displayTime;

struct taskDescriptor_s task_ledRedToggle;

struct taskDescriptor_s task_controlRedLed;

struct taskDescriptor_s task_compareAlarmClock;

struct taskDescriptor_s task_ledGreenToggle;

struct taskDescriptor_s task_CheckRotary;

void task_button_debouncing(void *a) {
	button_checkState();
}

void lcdDisplayClock(void *a) {

	systemTime_t tempTime = 0;

	tempTime = scheduler_getTime();
	clock_time.hour = (tempTime / HOUR_IN_MS) % 24;

	clock_time.minute = (tempTime / MIN_IN_MS) % 60;

	clock_time.second = (tempTime / SEC_IN_MS) % 60;

	lcd_setCursor(0, 0);
	fprintf(lcdout, "%2u:%2u:%2u", clock_time.hour, clock_time.minute,
			clock_time.second);
}

static void joystickPressedDispatch(void * param) {
	Event e = { .signal = JOYSTICK_PRESSED };
	fsm_dispatch(theFsm, &e);
}

static void rotaryPressedDispatch(void * param) {
	Event e = { . signal = ROTARY_PRESSED };
	fsm_dispatch(theFsm, &e);
}

static void rotaryClockWiseDispatch(void *param) {
	Event e = { . signal = ROTARY_CLOCKWISE };
	fsm_dispatch(theFsm, &e);
}

static void rotaryCounterClockWiseDispatch(void *param) {
	Event e = { . signal = ROTARY_ANTICLOCKWISE };
	fsm_dispatch(theFsm, &e);
}

void ledToggle_alarm(void *a) {
	led_redToggle();
}

void controlRedLed(void *a) {
	scheduler_remove(&task_ledRedToggle);
	led_redOff();
	scheduler_remove(&task_controlRedLed);
	/*
	 * if you make the task task_controlRedLed
	 * aperiodic then no need to add scheduler_remove(&task_controlRedLed); here theoretically
	 * also no need to add scheduler_remove(&task_controlRedLed); in clockRun after the button press
	 */
}

void checkRotaryPosition(void * a) {
	Rotary_Debounce();
}

void ledGreenBlink(void *a) {
	led_greenToggle();
}

void shouldAlarmGoOff(void *a) {

	if (clock_time.hour == alarm_time.hour
			&& clock_time.minute == alarm_time.minute
			&& clock_time.second == alarm_time.second
			&& theFsm->isAlarmEnabled) {
		scheduler_add(&task_controlRedLed);
		Event e = { . signal = ALARM_FIRED };
		fsm_dispatch(theFsm, &e);
	}

}

void displayClock(struct time_t myTime, bool showSeconds) {
	struct time_t tempTime = myTime;
	if (!showSeconds) {
		lcd_setCursor(0, 0);
		fprintf(lcdout, "%2u:%2u", tempTime.hour, tempTime.minute);
	} else {
		lcd_setCursor(0, 0);
		fprintf(lcdout, "%2u:%2u:%2u", tempTime.hour, tempTime.minute,
				tempTime.second);

	}
}

fsmReturnStatus setHourClock(Fsm *fsm, const Event *event) {
	lcd_setCursor(0, 1);
	fprintf(lcdout, "set Hours");

	switch (event->signal) {
	case ROTARY_CLOCKWISE:
		fsm->timeSet.hour++;
		displayClock(fsm->timeSet, false);
		return RET_HANDLED;
	case ROTARY_ANTICLOCKWISE:
		fsm->timeSet.hour--;
		displayClock(fsm->timeSet, false);
		return RET_HANDLED;
	case ROTARY_PRESSED:
		fsm->timeSet.hour++;
		if (fsm->timeSet.hour > 24) {
			fsm->timeSet.hour = 0;
		}
		displayClock(fsm->timeSet, false);
		return RET_HANDLED;
	case JOYSTICK_PRESSED:

		return TRANSITION(setMinuteClock);
	default:
		return RET_IGNORED;
	}
}

fsmReturnStatus setMinuteClock(Fsm *fsm, const Event *event) {
	lcd_setCursor(0, 1);
	fprintf(lcdout, "set Minutes");
	switch (event->signal) {
	case ROTARY_CLOCKWISE:
		fsm->timeSet.minute++;
		displayClock(fsm->timeSet, false);
		return RET_HANDLED;
	case ROTARY_ANTICLOCKWISE:
		fsm->timeSet.minute--;
		displayClock(fsm->timeSet, false);
		return RET_HANDLED;
	case ROTARY_PRESSED:
		fsm->timeSet.minute++;
		if (fsm->timeSet.minute > 60) {
			fsm->timeSet.minute = 0;
		}
		displayClock(fsm->timeSet, false);
		return RET_HANDLED;
	case JOYSTICK_PRESSED:
		lcd_clear();
		systemTime_t tempTime = fsm->timeSet.hour * HOUR_IN_MS
				+ fsm->timeSet.minute * MIN_IN_MS;
		scheduler_setTime(tempTime);
		scheduler_add(&task_displayTime);
		return TRANSITION(clockRun);
	default:
		return RET_IGNORED;
	}
}

fsmReturnStatus clockRun(Fsm *fsm, const Event *event) {

	switch (event->signal) {

	case ALARM_FIRED:
		scheduler_add(&task_ledRedToggle);
		return TRANSITION(alarmFired);

	case ROTARY_PRESSED:
		/* we toggle the mode of alarm here */
		if (fsm->isAlarmEnabled) {
			fsm->isAlarmEnabled = false;
			led_yellowOff();
		} else {
			fsm->isAlarmEnabled = true;
			led_yellowOn();
		}
		return RET_HANDLED;
		/* set the alarm time again*/
	case JOYSTICK_PRESSED:
		scheduler_remove(&task_displayTime);
		scheduler_remove(&task_compareAlarmClock);
		lcd_clear();
		return TRANSITION(setHourAlarm);
	default:
		return RET_IGNORED;
	}

}

fsmReturnStatus setHourAlarm(Fsm *fsm, const Event *event) {
	lcd_setCursor(0, 0);
	fprintf(lcdout, "%2u:%2u", alarm_time.hour, alarm_time.minute);
	lcd_setCursor(0, 1);
	fprintf(lcdout, "set alarm hour");
	switch (event->signal) {
	case ROTARY_CLOCKWISE:
		alarm_time.hour++;
		displayClock(alarm_time, false);
		return RET_HANDLED;
	case ROTARY_ANTICLOCKWISE:
		alarm_time.hour--;
		displayClock(alarm_time, false);
		return RET_HANDLED;
	case ROTARY_PRESSED:
		alarm_time.hour++;
		if (alarm_time.hour > 24) {
			alarm_time.hour = 0;
		}
		displayClock(alarm_time, false);
		return RET_HANDLED;
	case JOYSTICK_PRESSED:
		lcd_clear();
		return TRANSITION(setMinuteAlarm);
	default:
		return RET_IGNORED;
	}
}

fsmReturnStatus setMinuteAlarm(Fsm *fsm, const Event *event) {
	lcd_setCursor(0, 0);
	fprintf(lcdout, "%2u:%2u", alarm_time.hour, alarm_time.minute);
	lcd_setCursor(0, 1);
	fprintf(lcdout, "set alarm Minute");
	switch (event->signal) {
	case ROTARY_CLOCKWISE:
		alarm_time.minute++;
		displayClock(alarm_time, false);
		return RET_HANDLED;
	case ROTARY_ANTICLOCKWISE:
		alarm_time.minute--;
		displayClock(alarm_time, false);
		return RET_HANDLED;
	case ROTARY_PRESSED:
		alarm_time.minute++;
		if (alarm_time.minute > 60) {
			alarm_time.minute = 0;
		}
		displayClock(alarm_time, false);
		return RET_HANDLED;
	case JOYSTICK_PRESSED:
		scheduler_add(&task_displayTime);
		scheduler_add(&task_compareAlarmClock);
		lcd_clear();
		return TRANSITION(clockRun);
	default:
		return RET_IGNORED;
	}
}

fsmReturnStatus alarmFired(Fsm *fsm, const Event *event) {
	switch (event->signal) {
	/**
	 * if any of the button presses is detected
	 * the alarm is turned off and the transition to clockRun takes place
	 */
	case JOYSTICK_PRESSED:
	case ROTARY_PRESSED:
	case ROTARY_ANTICLOCKWISE:
	case ROTARY_CLOCKWISE:
		scheduler_remove(&task_ledRedToggle);
		led_redOff();
		return TRANSITION(clockRun);
	default:
		return RET_IGNORED;

	}

}

inline static void fsm_dispatch(Fsm* fsm, const Event* event) {
	static Event entryEvent = { .signal = ENTRY };
	static Event exitEvent = { .signal = EXIT };
	State s = fsm->state;
	fsmReturnStatus r = fsm->state(fsm, event);
	if (r == RET_TRANSITION) {
		s(fsm, &exitEvent); //< call exit action of last state
		fsm->state(fsm, &entryEvent); //< call entry action of new state
	}
}

inline static void fsm_init(Fsm* fsm, State init) {

	lcd_setCursor(0, 0);
	fprintf(lcdout, "HH:MM");

	theFsm = fsm;

	theFsm->timeSet.hour = 0;
	theFsm->timeSet.minute = 0;
	theFsm->timeSet.second = 0;
	theFsm->timeSet.milli = 0;

	theFsm->isAlarmEnabled = false;
	Event entryEvent = { .signal = ENTRY };
	theFsm->state = init;
	theFsm->state(theFsm, &entryEvent);
}

void clock_init() {

	/* Initialize the peripherals*/

	led_greenInit();
	led_redInit();
	led_yellowInit();
	lcd_init();
	lcd_clear();
	uart_init(BAUDRATE);

	led_greenOff();
	led_redOff();
	led_yellowOff();

	button_init(true);

	scheduler_init();
	rotary_init();
	button_setJoystickButtonCallback(joystickPressedDispatch);
	button_setRotaryButtonCallback(rotaryPressedDispatch);

	rotary_setClockwiseCallback(rotaryClockWiseDispatch);
	rotary_setCounterClockwiseCallback(rotaryCounterClockWiseDispatch);
	/**
	 * scheduling and setting of parameters of tasks happen here
	 */

	task_debouncing.execute = 0;
	task_debouncing.period = Period_task_debounce;
	task_debouncing.task = task_button_debouncing;
	task_debouncing.expire = Period_task_debounce;
	scheduler_add(&task_debouncing);

	task_displayTime.execute = 0;
	task_displayTime.period = 1000;
	task_displayTime.expire = 1000;
	task_displayTime.task = lcdDisplayClock;

	task_ledRedToggle.execute = 0;
	task_ledRedToggle.period = 250;
	task_ledRedToggle.expire = 250;
	task_ledRedToggle.task = ledToggle_alarm;

	task_controlRedLed.execute = 0;
	task_controlRedLed.period = 5000;
	task_controlRedLed.expire = 5000;
	task_controlRedLed.task = controlRedLed;

	task_compareAlarmClock.execute = 0;
	task_compareAlarmClock.period = 100;
	task_compareAlarmClock.expire = 100;
	task_compareAlarmClock.task = shouldAlarmGoOff;
	scheduler_add(&task_compareAlarmClock);

	task_ledGreenToggle.execute = 0;
	task_ledGreenToggle.period = 1000;
	task_ledGreenToggle.expire = 1000;
	task_ledGreenToggle.task = ledGreenBlink;
	scheduler_add(&task_ledGreenToggle);

	task_CheckRotary.task = checkRotaryPosition;
	task_CheckRotary.execute = 0;
	task_CheckRotary.expire = 5;
	task_CheckRotary.period = 5;
	scheduler_add(&task_CheckRotary);

	Fsm *fsm = malloc(sizeof(Fsm));
	State initialState = setHourClock;
	fsm_init(fsm, initialState);
}

