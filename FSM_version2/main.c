#include <avr/interrupt.h>
#include <ses_alarm.h>
#include <ses_rotary.h>
#include <ses_scheduler.h>

int main() {

	/* Initialize the FSM clock */
	clock_init();
	//led_greenOn();
	//led_redOn();
	/* Enable global interrupts */
	sei();
	/**
	 * All other necessary peripherals and the scheduler are
	 * initialized in ses_alarm.c
	 */


	for (;;) {
		scheduler_run();
	}
	return 0;
}
