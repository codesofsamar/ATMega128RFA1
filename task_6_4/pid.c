#include<ses_button.h>
#include<ses_led.h>
#include<ses_lcd.h>
#include<ses_uart.h>
#include<ses_adc.h>
#include<stdlib.h>
#include<ses_timer.h>
#include "ses_scheduler.h"
#include <util/atomic.h>
#include "ses_pwm.h"
#include "ses_common.h"
#include "ses_motorFrequency.h"

#define Period_TASK_DEBOUNCE 5

bool motor_state = false;

int PID_output = 0, integral_error = 0;
/* three different types of error*/
int p_error = 0, i_error = 0, d_error = 0;
int previous_error = 0;
int set_value = 0, current_value;

uint16_t counter_rotary = 0;
uint16_t counter_joystick = 0;
uint16_t pix_col = 0; pix_row = 0;

/* three different coefficients of PID*/
int K_p = 0, K_i = 2, K_d = 0;

/*
 * Implementation of the PID algorithm
 */
void pid_algorithm(void *a) {

	/* proportional, integral and derivative output*/
	int p_output = 0, i_output = 0, d_output = 0;

	int maxValue = pow(2, 16);
	int minValue = -pow(2, 16);

	current_value = motorFrequency_getRecent();

	/* calculation of three errors */
	p_error = set_value - current_value; // proportional error

	i_error = integral_error; // integral error

	if (i_error > maxValue) {
		i_error = maxValue;
	} else if (i_error < minValue) {
		i_error = minValue;
	}

	d_error = p_error - previous_error; // derivative error

	/* calculation of the three components of the PID output*/
	p_output = K_p * p_error;
	i_output = K_i * i_error;
	d_output = K_d * d_error;

	/* sum all the components of the error*/
	PID_output = p_output + i_output + d_output;

	/**
	 *  it is divided by 100 because the k_i is actually 0.02
	 *  but since we scaled it a division by 100 is necessary
	 */
	PID_output = PID_output / 100;
	if (PID_output > 255) {
		PID_output = 255;
	} else if (PID_output < 0) {
		PID_output = 0;
	}
	pwm_setDutyCycle(PID_output);

	/* update the previous error and the integral error*/
	previous_error = p_error;
	integral_error += p_error;

	lcd_setCursor(8, 0);
	fprintf(lcdout, "%u", motorFrequency_getRecent());


}

/**
 * sets the two different set points (freqencies)
 * at which we want to make our motor rotate
 */
void start_motor() {
	counter_rotary++;
	if (counter_rotary % 2 == 0) {
		lcd_clear();
		set_value = 100;
	} else {
		lcd_clear();
		set_value = 150;
	}

}

/**
 * to stop the motor
 * which is called from joystick callback
 */
void stop_motor() {

	set_value = 0;
}


/**
 * set the dutycycle to the motor
 * based on the output of the PID controller
 */
void set_dutyCycle(void *a) {
	pwm_setDutyCycle(PID_output);
}


/**
 * this function prints the trace of the recent frequency
 * to show the fluctuations of the frequencies
 */
void printTRACE(void *a) {

	uint16_t trace_value = motorFrequency_getRecent();

	trace_value = trace_value / 10;

	/* set vertical borders for plotting the trace*/
	for (int i = 0; i < 50; i++) {
		lcd_setPixel(i, 0, true);
		lcd_setPixel(i, 115, true);
	}

	/* set the horizontal border for plotting the trace*/
	for (int i = 0; i < 115; i++) {
		lcd_setPixel(31, i, true);
	}

	/* plot for the set frequency*/
	for (int i = 0; i < 115; i++) {
		lcd_setPixel(31 - set_value / 10, i, true);
	}

	/* plot the trace of frequency on the lcd*/
	lcd_setPixel(31 - trace_value, pix_col, true);
	pix_col++;
	if (pix_col == 115) {
		pix_col = 0;
		lcd_clear();
	}

}
void task_button_debouncing(void *a) {
	button_checkState();
}

void PRINT_REVOLUTIONS(void *a) {

	lcd_clear();

	uint16_t myRecent = motorFrequency_getRecent();
	uint16_t myMedian = motorFrequency_getMedian();

	lcd_setCursor(0, 0);
	fprintf(lcdout, "recent3/min: %u\n", myRecent);

	lcd_setCursor(0, 2);
	fprintf(lcdout, "Median/min : %u\n", myMedian);

	lcd_setCursor(0, 1);
	fprintf(lcdout, "myBool %d", returnMybool());

}

int main(void) {

	pwm_init();
	led_redInit();
	led_greenInit();
	led_yellowInit();

	led_greenOff();
	led_yellowOff();
	led_redOff();

	pwm_setDutyCycle(0);
	button_init(true);
	scheduler_init();
	motorFrequency_init();
	lcd_init();
	timer5_init();

	/* task that checks the button state every 5ms*/
	struct taskDescriptor_s TASK_DEBOUNCE;
	TASK_DEBOUNCE.execute = 0;
	TASK_DEBOUNCE.period = Period_TASK_DEBOUNCE;
	TASK_DEBOUNCE.task = task_button_debouncing;
	TASK_DEBOUNCE.expire = Period_TASK_DEBOUNCE;
	scheduler_add(&TASK_DEBOUNCE);

	struct taskDescriptor_s print_trace;
	print_trace.execute = 0;
	print_trace.period = 100; //every one tenth of a second
	print_trace.expire = 100;
	print_trace.task = printTRACE;
	scheduler_add(&print_trace);

	/* task to run the PID algorithm*/
	struct taskDescriptor_s run_pid;
	run_pid.execute = 0;
	run_pid.period = 100; //every one tenth of a second
	run_pid.expire = 100;
	run_pid.task = pid_algorithm;
	scheduler_add(&run_pid);

	/* call back to set duty cylce and toggle the state of motor*/
	button_setRotaryButtonCallback(start_motor);
	/**
	 * this stops the motor it would be nice to stop the motor before giving it another
	 * target frequency because when motor starts from 0 to its target frequency then
	 * we can clearly see the overshoot and it coming back to the target frequency
	 */
	button_setJoystickButtonCallback(stop_motor);

	/* enable the global interrupts */
	sei();

	for (;;) {
		scheduler_run();
	}

	return 0;
}

