#include<ses_button.h>
#include<ses_led.h>
#include<ses_lcd.h>
#include<ses_uart.h>
#include<ses_adc.h>
#include<stdlib.h>
#include<ses_timer.h>
#include "ses_scheduler.h"
#include <util/atomic.h>

uint32_t xx = 0;
uint16_t count = 0;
uint16_t yy = 0;
uint16_t counter_rotary = 0;

bool buttonInitState = true;

#define Period_TASK_TOGGLE 2000
#define Period_TASK_DEBOUNCE 5
#define Period_TASK_YELLOW_OFF 100
#define Period_STOP_WATCH 100
#define BAUDRATE 57600

void stop_watch(void *a) {
	if (counter_rotary % 2 == 1) {
		xx += 100; /* one tenth of a second */
	}
	lcd_setCursor(0, 1);
	fprintf(lcdout, "timer: %lu:%.1lu", xx / 1000, (xx % 1000) / 100);
	//fprintf(lcdout, "timer: %.1lu", xx/1000);
}
void task_led_green_toggle(void *a) {
	led_greenToggle();
}

void task_button_debouncing(void *a) {
	button_checkState();
}

void task_yellow_off(void *a) {
	if (count % 2 == 1) {
		yy += 100;
	} else {
		yy = 0;
	}

	if (yy >= 5000) {
		led_yellowOff();
		yy = 0;
		count++;
	}

}

void toggle_yellow(void) {
	led_yellowToggle();
	count++;
}

void toggle_red(void) {
	led_redToggle();
	counter_rotary++;
}


int main(void) {
	/*Initialize all the needed functions*/
	button_init(buttonInitState);
	led_greenInit();
	led_redInit();
	led_yellowInit();
	led_yellowOff();
	led_redOff();
	lcd_init();
	uart_init(BAUDRATE);
	scheduler_init();

	/* task that toggles the green LED every 2 sec*/
	struct taskDescriptor_s TASK_TOGGLE;
	TASK_TOGGLE.execute = 0;
	TASK_TOGGLE.period = Period_TASK_TOGGLE; //non-zero
	TASK_TOGGLE.task = task_led_green_toggle;
	TASK_TOGGLE.expire = Period_TASK_TOGGLE;
	scheduler_add(&TASK_TOGGLE);

	/* task that checks the button state every 5ms*/
	struct taskDescriptor_s TASK_DEBOUNCE;
	TASK_DEBOUNCE.execute = 0;
	TASK_DEBOUNCE.period = Period_TASK_DEBOUNCE; //Period1;
	TASK_DEBOUNCE.task = task_button_debouncing;
	TASK_DEBOUNCE.expire = Period_TASK_DEBOUNCE;
	scheduler_add(&TASK_DEBOUNCE);

	button_setJoystickButtonCallback(toggle_yellow);
	button_setRotaryButtonCallback(toggle_red);

	/* task that turns the yellow LED off after 5s it has been on*/
	struct taskDescriptor_s TASK_YELLOW_OFF_5S;
	TASK_YELLOW_OFF_5S.execute = 0;
	TASK_YELLOW_OFF_5S.period = Period_TASK_YELLOW_OFF;
	TASK_YELLOW_OFF_5S.task = task_yellow_off;
	TASK_YELLOW_OFF_5S.expire = Period_TASK_YELLOW_OFF;
	scheduler_add(&TASK_YELLOW_OFF_5S);

	/*task that updates the timer on stop watch*/
	struct taskDescriptor_s STOP_WATCH;
	STOP_WATCH.execute = 0;
	STOP_WATCH.period = Period_STOP_WATCH;
	STOP_WATCH.task = stop_watch;
	STOP_WATCH.expire = Period_STOP_WATCH;
	scheduler_add(&STOP_WATCH);

	/* enable global interrupts*/
	sei();

	/*run the scheduler_run in an infinite loop*/
	for (;;) {
		scheduler_run();
	}

	return 1;

}
